<?php

use yii\db\Migration;

/**
 * Handles the creation of table `filters`.
 */
class m181206_151346_create_filters_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bank_filters}}', [
            'id' => $this->primaryKey(),
            'filter' => $this->string()->notNull(),
            'category_id' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey(
            '{{%fk-bank_filter_category_id}}',
            '{{%bank_filters}}',
            'category_id',
            '{{%bank_categories}}',
            'id',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%fk-bank_filter_category_id}}', '{{%bank_filters}}');
        $this->dropTable('{{%bank_filters}}');
        return true;
    }
}
