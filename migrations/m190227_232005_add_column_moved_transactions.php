<?php

use yii\db\Migration;

class m190227_232005_add_column_moved_transactions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%bank_transactions}}', 'moved', $this->boolean()->defaultValue(false));
        $this->execute('UPDATE {{%bank_transactions}} SET moved = 1 WHERE date != date_fiscal');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%bank_transactions}}', 'moved');
        return true;
    }
}
