<?php

use yii\db\Migration;

class m190129_223956_add_colum_expense_invoices_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%bank_invoices}}', 'expense', $this->boolean()->defaultValue(true));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%bank_invoices}}', 'expense');
        return true;
    }
}
