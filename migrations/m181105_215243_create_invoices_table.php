<?php

use yii\db\Migration;

class m181105_215243_create_invoices_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bank_invoices}}', [
            'id' => $this->primaryKey(),
            'uid' => $this->string()->unique()->notNull(),
            'date' => $this->date()->notNull(),
            'description' => $this->string()->notNull(),
            'attachment' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bank_invoices}}');
        return true;
    }
}
