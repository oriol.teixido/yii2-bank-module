<?php

use yii\db\Migration;

/**
 * Handles the creation of table `amortizations`.
 */
class m181202_232834_create_amortizations_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bank_amortizations}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'start' => $this->date()->notNull(),
            'end' => $this->date(),
            'buy_category_id' => $this->integer(),
            'sell_category_id' => $this->integer(),
        ]);
        $this->createTable('{{%bank_amortizations_x_categories}}', [
            'amortization_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey(
            '{{%fk-bank_amortizations_x_categories_amortization_id}}',
            '{{%bank_amortizations_x_categories}}',
            'amortization_id',
            '{{%bank_amortizations}}',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
        $this->addForeignKey(
            '{{%fk-bank_amortizations_x_categories_category_id}}',
            '{{%bank_amortizations_x_categories}}',
            'category_id',
            '{{%bank_categories}}',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%fk-bank_amortizations_x_categories_amortization_id}}', '{{%bank_amortizations_x_categories}}');
        $this->dropForeignKey('{{%fk-bank_amortizations_x_categories_category_id}}', '{{%bank_amortizations_x_categories}}');
        $this->dropTable('{{%bank_amortizations_x_categories}}');
        $this->dropTable('{{%bank_amortizations}}');
        return true;
    }
}
