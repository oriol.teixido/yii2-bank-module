<?php

use yii\db\Migration;

class m181107_162915_add_colums_imports_invoices_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%bank_invoices}}', 'total', $this->money($precision = 19, $scale = 2)->defaultValue(0));
        $this->alterColumn('{{%bank_invoices}}', 'total', $this->money($precision = 19, $scale = 2)->notNull());
        $this->addColumn('{{%bank_invoices}}', 'vat', $this->money($precision = 19, $scale = 2)->defaultValue(0));
        $this->alterColumn('{{%bank_invoices}}', 'vat', $this->money($precision = 19, $scale = 2)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%bank_invoices}}', 'total');
        $this->dropColumn('{{%bank_invoices}}', 'vat');
        return true;
    }
}
