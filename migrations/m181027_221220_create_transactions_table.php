<?php

use yii\db\Migration;

class m181027_221220_create_transactions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bank_transactions}}', [
            'id' => $this->primaryKey(),
            'date' => $this->date()->notNull(),
            'value' => $this->money($precision = 19, $scale = 2)->notNull(),
            'description' => $this->text(),
            'validated' => $this->boolean()->notNull()->defaultValue(false),
            'account_id' => $this->integer()->notNull(),
            'category_id' => $this->integer(),
        ]);
        $this->addForeignKey(
            '{{%fk-bank_transactions-account}}',
            '{{%bank_transactions}}',
            'account_id',
            '{{%bank_accounts}}',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
        $this->addForeignKey(
            '{{%fk-bank_transactions-category}}',
            '{{%bank_transactions}}',
            'category_id',
            '{{%bank_categories}}',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
        $this->createIndex(
            '{{%idx-bank_transactions-date}}',
            '{{%bank_transactions}}',
            'date'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%fk-bank_transactions-account}}', '{{%bank_transactions}}');
        $this->dropForeignKey('{{%fk-bank_transactions-category}}', '{{%bank_transactions}}');
        $this->dropIndex('{{%idx-bank_transactions-date}}', '{{%bank_transactions}}');
        $this->dropTable('{{%bank_transactions}}');
        return true;
    }
}
