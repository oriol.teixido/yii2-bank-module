<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bank_fiscal_year_budgets`.
 */
class m190218_191934_create_fiscal_year_budgets_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bank_fiscal_year_budgets}}', [
            'id' => $this->primaryKey(),
            'income' => $this->money($precision = 19, $scale = 2)->notNull(),
            'expense' => $this->money($precision = 19, $scale = 2)->notNull(),
            'fiscal_year_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
        ]);
        $this->createIndex('idx_unique', '{{%bank_fiscal_year_budgets}}', ['fiscal_year_id', 'category_id'], $unique = true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx_unique', '{{%bank_fiscal_year_budgets}}');
        $this->dropTable('{{%bank_fiscal_year_budgets}}');
        return true;
    }
}
