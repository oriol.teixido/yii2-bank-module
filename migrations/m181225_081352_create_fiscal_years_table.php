<?php

use yii\db\Migration;

/**
 * Handles the creation of table `fiscal_years` and related attributes.
 */
class m181225_081352_create_fiscal_years_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%bank_transactions}}', 'date_fiscal', $this->date());
        $this->execute('UPDATE {{%bank_transactions}} SET date_fiscal = date');
        $this->alterColumn('{{%bank_transactions}}', 'date_fiscal', $this->date()->notNull());
        $this->addColumn('{{%bank_categories}}', 'balanced', $this->boolean()->notNull()->defaultValue(false));
        $this->createTable('{{%bank_fiscal_years}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'start' => $this->date()->notNull(),
            'end' => $this->date()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bank_fiscal_years}}');
        $this->dropColumn('{{%bank_categories}}', 'balanced');
        $this->dropColumn('{{%bank_transactions}}', 'date_fiscal');
        return true;
    }
}
