<?php

use yii\db\Migration;

class m200121_065915_add_column_iban_accounts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%bank_accounts}}', 'iban', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%bank_accounts}}', 'iban');
        return true;
    }
}
