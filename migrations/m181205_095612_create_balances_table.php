<?php

use yii\db\Migration;

/**
 * Handles the creation of table `balances`.
 */
class m181205_095612_create_balances_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bank_balances}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'accumulate' => $this->boolean()->notNull(),
            'inverse' => $this->boolean()->notNull(),
            'start' => $this->date(),
            'end' => $this->date(),
        ]);
        $this->createTable('{{%bank_balances_x_categories}}', [
            'balance_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(
            '{{%fk-bank_balances_x_categories_balance_id}}',
            '{{%bank_balances_x_categories}}',
            'balance_id',
            '{{%bank_balances}}',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
        $this->addForeignKey(
            '{{%fk-bank_balances_x_categories_category_id}}',
            '{{%bank_balances_x_categories}}',
            'category_id',
            '{{%bank_categories}}',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%fk-bank_balances_x_categories_category_id}}', '{{%bank_balances_x_categories}}');
        $this->dropForeignKey('{{%fk-bank_balances_x_categories_balance_id}}', '{{%bank_balances_x_categories}}');
        $this->dropTable('{{%bank_balances_x_categories}}');
        $this->dropTable('{{%bank_balances}}');
        return true;
    }
}
