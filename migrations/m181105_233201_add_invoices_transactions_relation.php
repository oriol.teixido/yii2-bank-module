<?php

use yii\db\Migration;

class m181105_233201_add_invoices_transactions_relation extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%bank_transactions}}', 'invoice_id', $this->integer()->defaultValue(null));
        $this->addForeignKey(
            '{{%fk-bank_transactions-invoices}}',
            '{{%bank_transactions}}',
            'invoice_id',
            '{{%bank_invoices}}',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%fk-bank_transactions-invoices}}', '{{%bank_transactions}}');
        $this->dropColumn('{{%bank_transactions}}', 'invoice_id');
        return true;
    }
}
