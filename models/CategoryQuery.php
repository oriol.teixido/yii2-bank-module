<?php

namespace oteixido\bank\models;

class CategoryQuery extends \yii\db\ActiveQuery
{
    public function balanced($balanced = true) {
        return $this->andWhere([Category::tableName().'.balanced' => $balanced]);
    }
}
