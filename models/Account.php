<?php

namespace oteixido\bank\models;

use Yii;

/**
 * This is the model class for table "bank_accounts".
 *
 * @property int $id
 * @property string $name
 * @property string $iban
 * @property string $description
 * @property float $balance
 *
 * @property Transaction[] $transactions
 */
class Account extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%bank_accounts}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string', 'max' => 64],
            ['name', 'trim'],
            ['name', 'unique'],
            ['iban', 'trim'],
            ['iban', 'string', 'max' => 64],
            ['description', 'trim'],
            ['description', 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('oteixido/bank', 'Name'),
            'iban' => Yii::t('oteixido/bank', 'IBAN'),
            'description' => Yii::t('oteixido/bank', 'Description'),
            'balance' => Yii::t('oteixido/bank', 'Balanç'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['account_id' => 'id']);
    }

    /**
     * @return float
     */
     public function getBalance()
     {
         return $this->getTransactions()->sum('value');
     }
}
