<?php

namespace oteixido\bank\models;

use Yii;

/**
 * This is the model class for table "bank_fiscal_years".
 *
 * @property int $id
 * @property string $name
 * @property date $start
 * @property date $end
 */
class FiscalYear extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%bank_fiscal_years}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string', 'max' => 255],
            ['name', 'unique'],
            ['start', 'required'],
            ['start', 'date', 'format' => 'yyyy-MM-dd'],
            ['end', 'required'],
            ['end', 'date', 'format' => 'yyyy-MM-dd'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('oteixido/bank', 'Nom'),
            'start' => Yii::t('oteixido/bank', 'Data d\'inici'),
            'end' => Yii::t('oteixido/bank', 'Data de final'),
            'income' => Yii::t('oteixido/bank', 'Ingressos'),
            'expense' => Yii::t('oteixido/bank', 'Despeses'),
            'balance' => Yii::t('oteixido/bank', 'Balanç'),
        ];
    }

    /**
     * @return float
     */
    public function getIncome()
    {
        return Transaction::find()->between($this->start, $this->end, 'date_fiscal')->income()->value();
    }

    /**
     * @return float
     */
    public function getExpense()
    {
        return -Transaction::find()->between($this->start, $this->end, 'date_fiscal')->expense()->value();
    }

    /**
     * @return float
     */
    public function getBalance()
    {
        return Transaction::find()->between($this->start, $this->end, 'date_fiscal')->value();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBudgets()
    {
        return $this->hasMany(FiscalYearBudget::className(), ['fiscal_year_id' => 'id']);
    }
}
