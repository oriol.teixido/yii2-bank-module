<?php

namespace oteixido\bank\models;

/**
 * This is the model class for table "bankbalances_x_categories".
 *
 * @property int category_id
 * @property int balance_id
 */
class BalanceXCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%bank_balances_x_categories}}';
    }
}
