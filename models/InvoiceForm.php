<?php

namespace oteixido\bank\models;

use yii\web\UploadedFile;

use Yii;

/**
 * This is the form class for model Invoce.
 *
 * @var $attachmentFile
 */
class InvoiceForm extends Invoice
{
    public $attachmentFile = false;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [ 'attachmentFile', 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf,jpg' ];
        return $rules;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'attachmentFile' => Yii::t('oteixido/bank', 'Fitxer adjunt'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function load($data, $formName = null)
    {
        if (!parent::load($data, $formName)) {
            return false;
        }
        $this->attachmentFile = UploadedFile::getInstance($this, 'attachmentFile');
        if ($this->attachmentFile) {
            $this->attachment =  $this->attachmentFile->name;

        }
        return true;
    }

    public function upload()
    {
        if ($this->attachmentFile === null) {
            return true;
        }
        return parent::uploadAttachment($this->attachmentFile);
    }

    public function removeAttachment()
    {
        parent::removeAttachment();
        $this->attachmentFile = null;
    }
}
