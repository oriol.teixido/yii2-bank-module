<?php

namespace oteixido\bank\models;

class TransactionMassiveForm extends \yii\base\Model
{
    const ACTION_VALIDATE = 'validate';
    const ACTION_DELETE = 'delete';
    const ACTION_FILTER = 'filters';

    public $action;
    public $selected = [];

    public function rules()
    {
        return [
            [ 'action', 'in', 'range' => [ self::ACTION_VALIDATE, self::ACTION_DELETE, self::ACTION_FILTER ] ],
            [ 'selected', 'safe' ],
        ];
    }
}
