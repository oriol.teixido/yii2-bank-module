<?php

namespace oteixido\bank\models;

use yii\data\ActiveDataProvider;

/**
 * AccountSearch represents the model behind the search form of `Account`.
 */
class AccountSearch extends Account
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'iban', 'description'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Account::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->defaultOrder = ['name' => SORT_ASC];
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        $query->andFilterWhere(['like', Account::tableName().'.name', $this->name]);
        $query->andFilterWhere(['like', Account::tableName().'.iban', $this->iban]);
        $query->andFilterWhere(['like', Account::tableName().'.description', $this->description]);
        return $dataProvider;
    }
}
