<?php

namespace oteixido\bank\models;

class FiscalYearBudgetQuery extends \yii\db\ActiveQuery
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function category($category)
    {
        return $this->andWhere([FiscalYearBudget::tableName().'.category_id' => $category->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function fiscalYear($fiscalYear)
    {
        return $this->andWhere([FiscalYearBudget::tableName().'.fiscal_year_id' => $fiscalYear->id]);
    }
}
