<?php

namespace oteixido\bank\models;

use yii\db\ActiveRecord;
use Yii;

use oteixido\helpers\StringHelper;

/**
 * This is the model class for table "bank_transactions".
 *
 * @property int $id
 * @property string $date_fiscal
 * @property string $date
 * @property float $value
 * @property string $description
 * @property int $account_id
 * @property int $category_id
 * @property int $invoice_id
 * @property boolean $validated
 * @property boolean $moved
 *
 * @property Account $account
 * @property Category $category
 * @property Invoice $invoice
 */
class Transaction extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%bank_transactions}}';
    }

    /**
     * {@inheritdoc}
     */
    public static function find()
    {
        return new TransactionQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['date', 'required'],
            ['date', 'date', 'format' => 'yyyy-MM-dd'],
            ['date_fiscal', 'date', 'format' => 'yyyy-MM-dd'],
            ['value', 'required'],
            ['value', 'trim'],
            ['value', 'number', 'numberPattern' => '/^[-+]?[0-9]+([\.,][0-9]+)?$/'],
            ['description', 'trim'],
            ['description', 'string', 'max' => 1000],
            ['account_id', 'required'],
            ['account_id', 'exist', 'skipOnError' => true, 'targetClass' => Account::className(), 'targetAttribute' => ['account_id' => 'id']],
            ['category_id', 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            ['invoice_id', 'exist', 'skipOnError' => true, 'targetClass' => Invoice::className(), 'targetAttribute' => ['invoice_id' => 'id']],
            ['validated', 'boolean'],
            ['account_id', 'filter', 'filter' => function($value) {
                return empty($value) ? null : $value;
            }],
            ['category_id', 'filter', 'filter' => function($value) {
                return empty($value) ? null : $value;
            }],
            ['invoice_id', 'filter', 'filter' => function($value) {
                return empty($value) ? null : $value;
            }],
            ['value', 'filter', 'filter' => function($value) {
                return str_replace(',', '.', $value);
            }],
            ['description', 'filter', 'filter' => function($value) {
                return StringHelper::normalize($value);
            }]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'date' => Yii::t('oteixido/bank', 'Data'),
            'date_fiscal' => Yii::t('oteixido/bank', 'Data fiscal'),
            'value' => Yii::t('oteixido/bank', 'Valor'),
            'description' => Yii::t('oteixido/bank', 'Descripció'),
            'account' => Yii::t('oteixido/bank', 'CCC'),
            'account_id' => Yii::t('oteixido/bank', 'CCC'),
            'category' => Yii::t('oteixido/bank', 'Categoria'),
            'category_id' => Yii::t('oteixido/bank', 'Categoria'),
            'invoice' => Yii::t('oteixido/bank', 'Factura'),
            'invoice_id' => Yii::t('oteixido/bank', 'Factura'),
            'validated' => Yii::t('oteixido/bank', 'Validat'),
            'moved' => Yii::t('oteixido/bank', 'Moguda'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if (empty($this->date_fiscal)) {
            $this->date_fiscal = $this->date;
        }
        $this->moved = ($this->date != $this->date_fiscal);

        return true;
    }
}
