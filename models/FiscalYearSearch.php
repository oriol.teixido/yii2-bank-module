<?php

namespace oteixido\bank\models;

use yii\data\ActiveDataProvider;

/**
 * FiscalYearSearch represents the model behind the search form of `FiscalYear`.
 */
class FiscalYearSearch extends FiscalYear
{
    /**
     * Creates data provider instance
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FiscalYear::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->defaultOrder = ['name' => SORT_ASC];
        return $dataProvider;
    }
}
