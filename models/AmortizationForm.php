<?php

namespace oteixido\bank\models;

use arogachev\ManyToMany\behaviors\ManyToManyBehavior;

class AmortizationForm extends Amortization
{
    public $categories_form;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = [
            [
                'class' => ManyToManyBehavior::className(),
                'relations' => [
                    [
                        'editableAttribute' => 'categories_form',
                        'table' => AmortizationXCategory::tableName(),
                        'ownAttribute' => 'amortization_id',
                        'relatedModel' => Category::className(),
                        'relatedAttribute' => 'category_id',
                    ],
                ],
            ],
        ];
        return array_merge(parent::behaviors(), $behaviors);
    }

    public function rules()
    {
        $rules = [
            ['categories_form', 'exist', 'targetClass' => Category::className(), 'targetAttribute' => 'id', 'allowArray' => true],
        ];
        return array_merge(parent::rules(), $rules);
    }
}
