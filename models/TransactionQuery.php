<?php

namespace oteixido\bank\models;

use yii\helpers\ArrayHelper;
use Yii;

class TransactionQuery extends \yii\db\ActiveQuery
{
    public function from($date, $attribute = 'date')
    {
        if (empty($date)) {
            return $this;
        }
        return $this->andWhere(['>=', Transaction::tableName().'.'.$attribute, $date]);
    }

    public function to($date, $attribute = 'date')
    {
        if (empty($date)) {
            return $this;
        }
        return $this->andWhere(['<=', Transaction::tableName().'.'.$attribute, $date]);
    }

    public function between($from, $to, $attribute = 'date')
    {
        return $this->from($from, $attribute)->to($to, $attribute);
    }

    public function income()
    {
        return $this->andWhere(['>', Transaction::tableName().'.value', 0]);
    }

    public function expense()
    {
        return $this->andWhere(['<', Transaction::tableName().'.value', 0]);
    }

    public function value()
    {
        return $this->sum('value');
    }

    public function validated($validated = true)
    {
        return $this->andWhere([Transaction::tableName().'.validated' => $validated]);
    }

    public function category($categories)
    {
        if (empty($categories)) {
            return $this;
        }
        $categories = is_array($categories) ? $categories : [ $categories ];
        $categoryIds = ArrayHelper::getColumn($categories, 'id');
        return $this->andWhere([Transaction::tableName().'.category_id' => $categoryIds]);
    }
}
