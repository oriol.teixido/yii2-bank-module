<?php

namespace oteixido\bank\models;

/**
 * This is the model class for table "bank_amortizations_x_categories".
 *
 * @property int category_id
 * @property int amortization_id
 */
class AmortizationXCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%bank_amortizations_x_categories}}';
    }
}
