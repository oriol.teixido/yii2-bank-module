<?php

namespace oteixido\bank\models;

use Yii;

/**
 * This is the model class for table "bank_amortizations".
 *
 * @property int $id
 * @property string $name
 * @property date $start
 * @property date $end
 *
 * @property Category $buy_category
 * @property Category $sell_category
 * @property Category[] $categories
 */
class Amortization extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%bank_amortizations}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string', 'max' => 255],
            ['name', 'unique'],
            ['start', 'required'],
            ['start', 'date', 'format' => 'yyyy-MM-dd'],
            ['end', 'date', 'format' => 'yyyy-MM-dd'],
            ['buy_category_id', 'required'],
            ['buy_category_id', 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['buy_category_id' => 'id']],
            ['sell_category_id', 'required'],
            ['sell_category_id', 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['sell_category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('oteixido/bank', 'Nom'),
            'start' => Yii::t('oteixido/bank', 'Inici'),
            'end' => Yii::t('oteixido/bank', 'Final'),
            'buy' => Yii::t('oteixido/bank', 'Import compra'),
            'sell' => Yii::t('oteixido/bank', 'Import venta'),
            'cost' => Yii::t('oteixido/bank', 'Import despeses'),
            'buy_category' => Yii::t('oteixido/bank', 'Categoria de compra'),
            'buy_category_id' => Yii::t('oteixido/bank', 'Categoria de compra'),
            'sell_category' => Yii::t('oteixido/bank', 'Categoria de venta'),
            'sell_category_id' => Yii::t('oteixido/bank', 'Categoria de venta'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])->
            viaTable(AmortizationXCategory::tableName(), ['amortization_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuy_category()
    {
        return $this->hasOne(Category::className(), ['id' => 'buy_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSell_category()
    {
        return $this->hasOne(Category::className(), ['id' => 'sell_category_id']);
    }

    public function getBuy()
    {
        return Transaction::find()->from($this->start)->to($this->end)->category($this->buy_category)->sum('value');
    }

    public function getSell()
    {
        return Transaction::find()->from($this->start)->to($this->end)->category($this->sell_category)->sum('value');
    }

    public function getCost()
    {
        return Transaction::find()->from($this->start)->to($this->end)->category($this->categories)->sum('value');
    }
}
