<?php

namespace oteixido\bank\models;

use Yii;

/**
 * This is the form class for model Transaction.
 *
 * @var $valueMin
 * @var $valueMax
 */
class TransactionForm extends Transaction
{
    public $valueMin = null;
    public $valueMax = null;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $rules = parent::rules();
        if ($this->valueMin !== null) {
            $rules[] = ['value', 'number', 'min' => $this->valueMin];
        }
        if ($this->valueMax !== null) {
            $rules[] = ['value', 'number', 'max' => $this->valueMax];
        }
        return $rules;
    }
}
