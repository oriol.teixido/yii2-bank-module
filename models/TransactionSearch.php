<?php

namespace oteixido\bank\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TransactionSearch represents the model behind the search form of `app\models\Transaction`.
 */
class TransactionSearch extends Transaction
{
    public $category;
    public $account;
    public $invoice;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'date_fiscal', 'description', 'account', 'category', 'invoice', 'validated', 'moved'], 'safe'],
            ['value', 'filter', 'filter' => function($value) {
                return str_replace(',', '.', $value);
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
     public function init()
     {
         $this->validated = null;
         $this->moved = null;
     }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Transaction::find();
        $query->joinWith(['account','category','invoice']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['category'] = [
            'asc' => [Category::tableName().'.name' => SORT_ASC],
            'desc' => [Category::tableName().'.name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['account'] = [
            'asc' => [Account::tableName().'.name' => SORT_ASC],
            'desc' => [Account::tableName().'.name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['invoice'] = [
            'asc' => [Invoice::tableName().'.id' => SORT_ASC],
            'desc' => [Invoice::tableName().'.id' => SORT_DESC],
        ];
        $dataProvider->sort->defaultOrder = ['date' => SORT_DESC, 'value' => SORT_DESC];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([Transaction::tableName().'.value' => $this->value]);
        $query->andFilterWhere([Transaction::tableName().'.date' => $this->date]);
        $query->andFilterWhere([Transaction::tableName().'.date_fiscal' => $this->date_fiscal]);
        $query->andFilterWhere([Transaction::tableName().'.validated' => $this->validated]);
        $query->andFilterWhere([Transaction::tableName().'.moved' => $this->moved]);
        $query->andFilterWhere(['like', Transaction::tableName().'.description', $this->description]);
        $query->andFilterWhere(['like', Category::tableName().'.name', $this->category]);
        $query->andFilterWhere(['like', Account::tableName().'.name', $this->account]);
        if ($this->invoice) {
            $query->andFilterWhere(['not', [Invoice::tableName().'.attachment' => null]]);
        }
        return $dataProvider;
    }
}
