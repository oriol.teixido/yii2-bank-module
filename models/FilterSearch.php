<?php

namespace oteixido\bank\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

use oteixido\helpers\StringHelper;

/**
 * FilterSearch represents the model behind the search form of `oteixido\bank\models\Filter`.
 */
class FilterSearch extends Filter
{
    public $category;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['category', 'safe'],
            ['filter', 'filter', 'filter' => function($value) {
                return StringHelper::normalize($value);
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Filter::find();
        $query->joinWith(['category']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->attributes['category'] = [
            'asc' => ['categories.name' => SORT_ASC],
            'desc' => ['categories.name' => SORT_DESC],
        ];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', Category::tableName().'.name', $this->category]);
        $query->andFilterWhere(['like', Filter::tableName().'.filter', $this->filter]);

        return $dataProvider;
    }
}
