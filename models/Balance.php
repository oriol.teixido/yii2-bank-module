<?php

namespace oteixido\bank\models;

use Yii;

/**
 * This is the model class for table "bank_balances".
 *
 * @property int $id
 * @property string $name
 * @property boolean $inverse
 * @property boolean $accumulate
 *
 * @property Category[] $categories
 */
class Balance extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%bank_balances}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string', 'max' => 255],
            ['name', 'unique'],
            ['start', 'date', 'format' => 'yyyy-MM-dd'],
            ['end', 'date', 'format' => 'yyyy-MM-dd'],
            ['accumulate', 'required'],
            ['accumulate', 'boolean'],
            ['inverse', 'required'],
            ['inverse', 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('oteixido/bank', 'Nom'),
            'accumulate' => Yii::t('oteixido/bank', 'Mostrar acumulat'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])->
            viaTable(BalanceXCategory::tableName(), ['balance_id' => 'id']);
    }
}
