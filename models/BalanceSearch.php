<?php

namespace oteixido\bank\models;

use yii\data\ActiveDataProvider;

/**
 * BalanceSearch represents the model behind the search form of `Balance`.
 */
class BalanceSearch extends Balance
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Balance::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->defaultOrder = ['name' => SORT_ASC];
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        $query->andFilterWhere(['like', Balance::tableName().'.name', $this->name]);
        return $dataProvider;
    }
}
