<?php

namespace oteixido\bank\models;

use Yii;

/**
 * This is the model class for table "bank_categories".
 *
 * @property int $id
 * @property string $name
 * @property bool $balanced
 *
 * @property Transactions[] $transactions
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%bank_categories}}';
    }

    /**
     * {@inheritdoc}
     */
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'trim'],
            ['name', 'string', 'max' => 255],
            ['name', 'unique'],
            ['balanced', 'required'],
            ['balanced', 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('oteixido/bank', 'Nom'),
            'balanced' => Yii::t('oteixido/bank', 'Balancejat'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['category' => 'name']);
    }
}
