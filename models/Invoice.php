<?php

namespace oteixido\bank\models;

use yii\db\ActiveRecord;
use Yii;

use oteixido\helpers\StringHelper;

/**
 * This is the model class for table "bank_invoices".
 *
 * @property int $id
 * @property string $uid
 * @property string $date
 * @property string $description
 * @property string $attachment
 * @property float $total
 * @property float $vat
 * @property boolean $expense
 *
 * @property Transaction[] $transactions
 */
class Invoice extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%bank_invoices}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['date', 'required'],
            ['date', 'date', 'format' => 'yyyy-MM-dd'],
            ['description', 'trim'],
            ['description', 'string', 'max' => 1000],
            ['total', 'required'],
            ['total', 'trim'],
            ['total', 'number', 'numberPattern' => '/^[-+]?[0-9]+([\.,][0-9]+)?$/'],
            ['vat', 'required'],
            ['vat', 'trim'],
            ['vat', 'number', 'numberPattern' => '/^[-+]?[0-9]+([\.,][0-9]+)?$/'],
            ['attachment', 'required'],
            ['expense', 'required'],
            ['expense', 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        if (empty($this->uid)) {
            $this->uid = StringHelper::uid();
        }
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'date' => Yii::t('oteixido/bank', 'Data'),
            'name' => Yii::t('oteixido/bank', 'Nom'),
            'description' => Yii::t('oteixido/bank', 'Descripció'),
            'attachment' => Yii::t('oteixido/bank', 'Fitxer adjunt'),
            'total' => Yii::t('oteixido/bank', 'Total'),
            'vat' => Yii::t('oteixido/bank', 'IVA'),
            'expense' => Yii::t('oteixido/bank', 'Despesa'),
        ];
    }

    public function getName()
    {
        return $this->date.' - '.$this->description . ' (' . Yii::$app->formatter->asCurrency($this->total) . ')';
    }

    public function getPaid()
    {
        return $this->getTransactions()->sum('value');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getAttachmentPath()
    {
        return $this->uploadPath().'/'.$this->uid;
    }

    /**
     * {@inheritdoc}
     */
    public function afterDelete()
    {
        $this->removeAttachment();
        parent::afterDelete();
    }

    /**
     * @param $file \yii\web\UploadedFile
     * @return boolean
     */
    public function uploadAttachment($file)
    {
        $result = $file->saveAs($this->getAttachmentPath());
        if (!$result) {
            $this->attachment = null;
        }
        return $result;
    }

    public function removeAttachment()
    {
        if (file_exists($this->getAttachmentPath())) {
            @unlink($this->getAttachmentPath());
        }
        $this->attachment = null;
    }

    private function uploadPath()
    {
        return Yii::$app->controller->module->params['invoices.upload.path'];
    }
}
