<?php

namespace oteixido\bank\models;

use Yii;

/**
 * This is the model class for table "fiscal_year_budgets".
 *
 * @property int $id
 * @property float $income
 * @property float $expense
 *
 * @property float $currentIncome
 * @property float $currentExpense
 *
 * @property FiscalYear $fiscalYear
 * @property Category $category
 */
class FiscalYearBudget extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%bank_fiscal_year_budgets}}';
    }

    /**
     * {@inheritdoc}
     */
    public static function find()
    {
        return new FiscalYearBudgetQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['income', 'required'],
            ['income', 'trim'],
            ['income', 'number', 'numberPattern' => '/^[-+]?[0-9]+([\.,][0-9]+)?$/'],
            ['expense', 'required'],
            ['expense', 'trim'],
            ['expense', 'number', 'numberPattern' => '/^[-+]?[0-9]+([\.,][0-9]+)?$/'],
            ['category_id', 'required'],
            ['category_id', 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            ['fiscal_year_id', 'required'],
            ['fiscal_year_id', 'exist', 'skipOnError' => true, 'targetClass' => FiscalYear::className(), 'targetAttribute' => ['fiscal_year_id' => 'id']],
            ['category_id', 'unique', 'targetAttribute' => ['category_id', 'fiscal_year_id'], 'message' =>  Yii::t('oteixido/bank', 'La categoria ja existeix.')],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('oteixido/bank', 'Nom'),
            'income' => Yii::t('oteixido/bank', 'Ingressos previstos'),
            'expense' => Yii::t('oteixido/bank', 'Despeses previstes'),
            'currentIncome' => Yii::t('oteixido/bank', 'Ingressos actuals'),
            'currentExpense' => Yii::t('oteixido/bank', 'Despeses actuals'),
            'category_id' => Yii::t('oteixido/bank', 'Categoria'),
            'fiscal_year_id' => Yii::t('oteixido/bank', 'Any fiscal'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiscalYear()
    {
        return $this->hasOne(FiscalYear::className(), ['id' => 'fiscal_year_id']);
    }

    /**
     * @return float
     */
    public function getCurrentExpense()
    {
        return -(float)Transaction::find()->where(['<', 'value', 0])->category($this->category)->between($this->fiscalYear->start, $this->fiscalYear->end, 'date_fiscal')->value();
    }

    /**
     * @return float
     */
    public function getCurrentIncome()
    {
        return (float)Transaction::find()->where(['>', 'value', 0])->category($this->category)->between($this->fiscalYear->start, $this->fiscalYear->end, 'date_fiscal')->value();
    }
}
