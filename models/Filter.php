<?php

namespace oteixido\bank\models;

use oteixido\helpers\StringHelper;

use Yii;

/**
 * This is the model class for table "bank_filters".
 *
 * @property int $id
 * @property string $filter
 * @property int $category_id
 *
 * @property Category $category
 */
class Filter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%bank_filters}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['filter', 'category_id'], 'required'],
            ['category_id', 'integer'],
            ['filter', 'string', 'max' => 255],
            ['filter', 'trim'],
            ['category_id', 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'filter' => Yii::t('oteixido/bank', 'Filtre'),
            'category_id' => Yii::t('oteixido/bank', 'Categoria'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        $this->filter = StringHelper::normalize($this->filter);
        return true;
    }
}
