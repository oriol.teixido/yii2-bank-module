<?php

namespace oteixido\bank\models;

class TransactionImportForm extends \yii\base\Model
{
    public $file;

    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => ['csv'], 'checkExtensionByMimeType'  => false],
        ];
    }
}
