<?php

namespace oteixido\bank\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * InvoiceSearch represents the model behind the search form of `app\models\Invoice`.
 */
class InvoiceSearch extends Invoice
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'description', 'vat', 'total', 'expense'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Invoice::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->defaultOrder = ['date' => SORT_DESC ];
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        $query->andFilterWhere(['like', Invoice::tableName().'.date', $this->date]);
        $query->andFilterWhere(['like', Invoice::tableName().'.description', $this->description]);
        $query->andFilterWhere(['=', Invoice::tableName().'.vat', $this->vat]);
        $query->andFilterWhere(['=', Invoice::tableName().'.total', $this->total]);
        $query->andFilterWhere(['=', Invoice::tableName().'.expense', $this->expense]);

        return $dataProvider;
    }
}
