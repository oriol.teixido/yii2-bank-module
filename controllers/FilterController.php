<?php

namespace oteixido\bank\controllers;

use oteixido\bank\models\Filter;
use oteixido\bank\models\FilterSearch;

class FilterController extends BaseController
{
    public function createModel()
    {
        return new Filter();
    }

    public function createSearchModel()
    {
        return new FilterSearch();
    }

    public function lookupModel($id)
    {
        return Filter::findOne($id);
    }
}
