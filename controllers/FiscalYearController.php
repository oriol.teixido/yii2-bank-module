<?php

namespace oteixido\bank\controllers;

use Yii;
use yii\data\ActiveDataProvider;

use oteixido\bank\models\FiscalYear;
use oteixido\bank\models\FiscalYearSearch;
use oteixido\bank\models\FiscalYearBudget;
use oteixido\bank\models\Category;

class FiscalYearController extends BaseController
{
    public function createModel()
    {
        return new FiscalYear();
    }

    public function createSearchModel()
    {
        return new FiscalYearSearch();
    }

    public function lookupModel($id)
    {
        return FiscalYear::findOne($id);
    }

    /**
     * Displays a single FiscalYear model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $dataProvider = new ActiveDataProvider([
            'query' => $model->getBudgets()->joinWith('category'),
            'pagination' => false,
            'sort' => [
                'attributes' => [
                    'category' => [
                        'asc' => [ Category::tableName().'.name' => SORT_ASC ],
                        'desc' => [ Category::tableName().'.name' => SORT_DESC ],
                    ],
                ],
                'defaultOrder' => [
                    'category' => SORT_ASC,
                ],
            ]
        ]);
        return $this->render('view', [
            'model' => $model,
            'budget' => $this->getNewBudget($model),
            'categories' => $this->getCategoriesWithoutBudget($model),
            'dataProvider' => $dataProvider,
        ]);
    }

    private function getCategoriesWithoutBudget($model)
    {
        return array_filter(Category::find()->orderBy(['name' => SORT_ASC])->balanced(false)->all(), function($category) use ($model) {
            return FiscalYearBudget::find()->category($category)->fiscalYear($model)->one() == null;
        });
    }

    private function getNewBudget($model)
    {
        $budget = new FiscalYearBudget();
        $budget->fiscal_year_id = $model->id;
        if ($budget->load(Yii::$app->request->post()) && $budget->save()) {
            $budget = new FiscalYearBudget();
            $budget->fiscal_year_id = $model->id;
        }
        return $budget;
    }

    public function actionUpdateBudget($id)
    {
        $model = $this->findBudgetModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update-budget', [
            'model' => $model,
            'categories' => array_merge($this->getCategoriesWithoutBudget($model), [ $model->category ]),
        ]);
    }

    public function actionDeleteBudget($id)
    {
        $budget = $this->findBudgetModel($id);
        $id = $budget->fiscalYear->id;
        $budget->delete();
        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Finds the model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Filter the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    private function findBudgetModel($id)
    {
        if (($model = FiscalYearBudget::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
