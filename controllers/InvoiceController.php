<?php

namespace oteixido\bank\controllers;

use yii\helpers\FileHelper;
use Yii;

use oteixido\bank\models\InvoiceForm;
use oteixido\bank\models\InvoiceSearch;

class InvoiceController extends BaseController
{
    public function createModel()
    {
        return new InvoiceForm();
    }

    public function createSearchModel()
    {
        return new InvoiceSearch();
    }

    public function lookupModel($id)
    {
        return InvoiceForm::findOne($id);
    }

    /**
     * Send attachment file content if exists.
     * @param integer $id
     * @param boolean $inline
     * @return string
     * @throws NotFoundHttpException if the model or the attachment cannot be found
     */
    public function actionAttachment($id, $inline = true)
    {
        $model = $this->findModel($id);
        if (!$model->attachment) {
            throw new NotFoundHttpException('The requested attachment does not exist.');
        }
        return Yii::$app->response->sendFile($model->getAttachmentPath(), $model->attachment, [
            'inline' => $inline,
            'mimeType' => FileHelper::getMimeTypeByExtension($model->attachment),
        ]);
    }

    public function beforeSave($model)
    {
        if (!$model->upload()) {
            Yii::$app->session->addFlash('danger', Yii::t('oteixido/bank', 'No s\'ha pogut desar l\'arxiu adjunt.'));
            return false;
        }
        return true;
    }
}
