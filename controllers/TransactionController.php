<?php

namespace oteixido\bank\controllers;

use yii\web\Response;
use yii\web\UploadedFile;
use Yii;

use oteixido\bank\models\TransactionForm;
use oteixido\bank\models\TransactionSearch;
use oteixido\bank\models\TransactionImportForm;
use oteixido\bank\models\TransactionMassiveForm;
use oteixido\bank\services\FilterService;

/**
 * TransactionController implements the CRUD actions for Transaction model.
 */
class TransactionController extends BaseController
{
    public function createModel()
    {
        $model = new TransactionForm();
        $model->validated = false;
        return $model;
    }

    public function createSearchModel()
    {
        return new TransactionSearch();
    }

    public function lookupModel($id)
    {
        return TransactionForm::findOne($id);
    }

    /**
     * Execute massive actions.
     * @return mixed
     */
    public function actionMassive()
    {
        $model = new TransactionMassiveForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            switch($model->action) {
                case TransactionMassiveForm::ACTION_FILTER:
                    $this->massiveFilter($model);
                    break;
                case TransactionMassiveForm::ACTION_VALIDATE:
                    $this->massiveValidate($model);
                    break;
                case TransactionMassiveForm::ACTION_DELETE:
                    $this->massiveDelete($model);
                    break;
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Import Transaction models.
     * @return mixed
     */
    public function actionImport()
    {
        $model = new TransactionImportForm();
        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->validate()) {
                $result = $this->getModule()->importer->importFromFile($model->file->tempName, $save = true);
                if ($result->isSuccess()) {
                    Yii::$app->session->addFlash('success', Yii::t('oteixido/bank', 'Importació realitzada amb èxit ({count} elements).', ['count' => $result->count()]));
                    return $this->redirect(['index']);
               }
               else {
                   foreach($result->getErrors() as $line => $errors) {
                       Yii::$app->session->addFlash('danger', Yii::t('oteixido/bank', 'Línia {line}: ', ['line' => $line]) . implode(' | ', $errors));
                    }
                }
            }
        }
        return $this->render('import', ['model' => $model]);
    }

    /**
     * Duplicate a new Transaction model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDuplicate($id)
    {
        $model = $this->duplicateModel($this->findModel($id));
        $model->validated = false;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Split a Transaction model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionSplit($id)
    {
        $model = $this->findModel($id);
        $modelNew = $this->duplicateModel($model);
        $modelNew->value = '';
        $modelNew->category_id = '';
        $modelNew->valueMin = $model->value < 0 ? $model->value : 0;
        $modelNew->valueMax = $model->value < 0 ? 0 : $model->value;
        $modelNew->validated = false;
        if ($modelNew->load(Yii::$app->request->post()) && $modelNew->save()) {
            $model->value = $model->value - $modelNew->value;
            $model->validated = false;
            $model->save($runValidation = false, ['value', 'validated']);
            return $this->redirect(['index']);
        }
        return $this->render('split', [
            'model' => $model,
            'modelNew' => $modelNew,
        ]);
    }

    private function massiveValidate($form)
    {
        foreach ($form->selected as $id) {
            $model = $this->lookupModel($id);
            $model->validated = true;
            $model->save($runValidation = false, ['validated']);
        }
        Yii::$app->session->addFlash('success',  Yii::t('oteixido/bank', 'S\'han validat {count} transaccions.', ['count' => count($form->selected)]));
    }

    private function massiveDelete($form)
    {
        foreach ($form->selected as $id) {
            $model = $this->lookupModel($id);
            $model->delete();
        }
        Yii::$app->session->addFlash('success',  Yii::t('oteixido/bank', 'S\'han esborrat {count} transaccions.', ['count' => count($form->selected)]));
    }

    private function massiveFilter($form)
    {
        $count = 0;
        $filterService = new FilterService();
        foreach ($form->selected as $id) {
            $model = $this->lookupModel($id);
            if ($filterService->setCategory($model)) {
                $count++;
            }
        }
        Yii::$app->session->addFlash('success', Yii::t('oteixido/bank', 'S\'han assignat {count} transaccions.', ['count' => $count]));
    }

    private function duplicateModel($model)
    {
        $model2 = $this->createModel();
        $model2->date = $model->date;
        $model2->value = $model->value;
        $model2->description = $model->description;
        $model2->account_id = $model->account_id;
        $model2->category_id = $model->category_id;
        return $model2;
    }

    private function getModule()
    {
        return Yii::$app->controller->module;
    }
}
