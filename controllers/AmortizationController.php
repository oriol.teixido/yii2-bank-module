<?php

namespace oteixido\bank\controllers;

use Yii;

use oteixido\bank\models\AmortizationForm;
use oteixido\bank\models\AmortizationSearch;
use oteixido\bank\models\TransactionSearch;

class AmortizationController extends BaseController
{
    public function createModel()
    {
        return new AmortizationForm();
    }

    public function createSearchModel()
    {
        return new AmortizationSearch();
    }

    public function lookupModel($id)
    {
        return AmortizationForm::findOne($id);
    }

    /**
     * Displays a single Amortization model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $searchModel = new TransactionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->between($model->start, $model->end)->category($model->categories);
        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
}
