<?php

namespace oteixido\bank\controllers;

use oteixido\bank\models\Account;
use oteixido\bank\models\AccountSearch;

class AccountController extends BaseController
{
    public function createModel()
    {
        return new Account();
    }

    public function createSearchModel()
    {
        return new AccountSearch();
    }

    public function lookupModel($id)
    {
        return Account::findOne($id);
    }
}
