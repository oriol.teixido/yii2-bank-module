<?php

namespace oteixido\bank\controllers;

use oteixido\bank\models\BalanceForm;
use oteixido\bank\models\BalanceSearch;
use oteixido\bank\components\BalanceDataProvider;

class BalanceController extends BaseController
{
    public function createModel()
    {
        return new BalanceForm();
    }

    public function createSearchModel()
    {
        return new BalanceSearch();
    }

    public function lookupModel($id)
    {
        return BalanceForm::findOne($id);
    }

    /**
     * Displays a single Balance model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $dataProvider = new BalanceDataProvider([
            'categories' => $model->categories,
            'from' => $model->start,
            'to' => $model->end,
            'scale' => $model->inverse ? -1.0 : 1.0,
            'sort' => [
                'defaultOrder' => [ 'year' => SORT_DESC ],
                'attributes' => ['year' ],
            ],
        ]);
        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }
}
