<?php

namespace oteixido\bank\controllers;

use oteixido\bank\models\Category;
use oteixido\bank\models\CategorySearch;

class CategoryController extends BaseController
{
    public function createModel()
    {
        return new Category();
    }

    public function createSearchModel()
    {
        return new CategorySearch();
    }

    public function lookupModel($id)
    {
        return Category::findOne($id);
    }
}
