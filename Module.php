<?php

namespace oteixido\bank;

use yii\i18n\PhpMessageSource;
use Yii;

/**
 * yii2-bank module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'oteixido\bank\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        Yii::$app->i18n->translations['oteixido/bank'] = [
            'class' => PhpMessageSource::className(),
            'sourceLanguage' => 'ca-ES',
            'basePath' => '@app/vendor/oteixido/yii2-bank-module/messages'
        ];
    }
}
