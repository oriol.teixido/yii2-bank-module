<?php

namespace oteixido\bank\components;

use yii\data\ArrayDataProvider;

use oteixido\bank\models\Transaction;

class BalanceDataProvider extends ArrayDataProvider
{
    public $from = null;
    public $to = null;
    public $categories = [];
    public $scale = 1.0;

    public function init()
    {
        if ($this->allModels === null) {
            $this->allModels = $this->loadBalance();
        }
    }

    private function loadBalance()
    {
        $from = !empty($this->from) ? $this->from : Transaction::find()->min(Transaction::tableName().'.date');
        $to = !empty($this->to) ? $this->to : Transaction::find()->max(Transaction::tableName().'.date');
        $fromYear = self::getYearFromDate($from);
        $toYear = self::getYearFromDate($to);

        $result = [];
        $yearTotal = 0.0;
        for($year=$fromYear; $year<=$toYear; $year++) {
            $yearValue = self::getYearValue($this->categories, $year) * $this->scale;
            $yearTotal += $yearValue;
            $values = [
                'year' => $year,
                'value' => $yearValue,
                'total' => $yearTotal,
            ];
            for($month=1; $month<=12; $month++) {
                $values[$month] = self::getMonthValue($this->categories, $year, $month) * $this->scale;
            }
            $result[] = $values;
        }
        return $result;
    }

    private function getMonthValue($categories, $year, $month) {
        $from = self::getFirstDay($month, $year);
        $to = self::getLastDay($month, $year);
        return (float)Transaction::find()->category($categories)->between($from, $to)->sum('value');
    }

    private function getYearValue($categories, $year) {
        $from = self::getFirstDay('01', $year);
        $to = self::getLastDay('12', $year);
        return (float)Transaction::find()->category($categories)->between($from, $to)->sum('value');
    }

    private function getYearFromDate($str)
    {
        $date = \DateTime::createFromFormat('Y-m-d', $str);
        return $date->format('Y');
    }

    private function getFirstDay($month, $year)
    {
        $day = '01';
        return "$year-$month-$day";
    }

    private function getLastDay($month, $year)
    {
        $day = date('t', strtotime(self::getFirstDay($month, $year)));
        return "$year-$month-$day";
    }
}
