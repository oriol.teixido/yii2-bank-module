Yii2 Bank Module
================
Yii2 Bank Module

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist oteixido/yii2-bank-module "*"
```

or add

```
"oteixido/yii2-bank-module": "*"
```

to the require section of your `composer.json` file.

Configuration
-------------

```php
'modules' => [
    ...
    'gridview' => [
        'class' => 'kartik\grid\Module',
    ],    
    'bank' => [
        'class' => 'oteixido\bank\Module',
        'params' => [
            'invoices.upload.path' => '/uploads/invoices'
        ],
        'components' => [
            'importer' => [
                /*
                 * Example of CSV file
                 * 2018-05-01;-600,95;Transferencia recibida;CE;Transferencia
                 */
                'class' => 'oteixido\importer\ImporterCSV',
                'classname' => 'oteixido\bank\models\Transaction',
                'attributes' => [
                    [
                        'attribute' => 'date'
                    ],
                    [
                        'attribute' => 'value',
                        'filters' => [
                            '/\./' => '',
                            '/,/' => '.',
                        ],
                    ],
                    [
                        'attribute' => 'description',
                    ],
                    [
                        'attribute' => 'account_id',
                        'foreignClassname' => 'oteixido\bank\models\Account',
                        'foreignAttribute' => 'name',
                        'foreignKey' => 'id',
                    ],
                ],
            ],
        ],
    ],
    ...
]
```

Migrations
----------

```bash
./yii migrate --migrationPath=vendor/oteixido/yii2-bank-module/migrations/
```
