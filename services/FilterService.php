<?php

namespace oteixido\bank\services;

use yii\base\Component;

use oteixido\bank\models\Filter;

class FilterService extends Component
{
    public $overwrite = false;
    private $filters = null;

    public function init()
    {
        $this->filters = Filter::find()->all();
    }

    public function setCategory($model)
    {
        if ($model->category_id != null && !$this->overwrite) {
            return false;
        }

        $filter = $this->getCategory($model->description);
        if ($filter == null) {
            return false;
        }
        $model->category_id = $filter->category_id;
        $model->save($runValidation = false, ['category_id']);
        return true;
    }

    private function getCategory($text)
    {
        foreach($this->filters as $filter) {
            if (strpos($text, $filter->filter) !== false) {
                return $filter;
            }
        }
        return null;
    }
}
