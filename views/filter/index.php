<?php

use yii\helpers\Html;
use yii\grid\GridView;

use oteixido\gui\widgets\ModelActionsWidget;
use oteixido\gui\components\ModelActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FilterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('oteixido/bank', 'Filtres');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filter-index">
    <?= ModelActionsWidget::widget() ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'filter',
            [
                'attribute' => 'category.name',
                'value' => 'category.name',
            ],
            [ 'class' => ModelActionColumn::className() ],
        ],
    ]); ?>
</div>
