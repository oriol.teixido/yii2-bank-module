<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Filter */

$this->title = Yii::t('oteixido/bank', 'Crear');
$this->params['breadcrumbs'][] = ['label' => Yii::t('oteixido/bank', 'Filtres'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filter-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
