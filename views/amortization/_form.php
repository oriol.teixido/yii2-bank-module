<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use oteixido\gui\widgets\buttons\SubmitButtonWidget;
use oteixido\gui\widgets\ModelActionsWidget;

use oteixido\bank\models\Category;

/* @var $this yii\web\View */
/* @var $model app\models\Amortization */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="amortization-form">
    <?= ModelActionsWidget::widget(['model' => $model]) ?>
    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'start')->widget(DatePicker::class, [
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>
    <?= $form->field($model, 'end')->widget(DatePicker::class, [
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>
    <?= $form->field($model, 'buy_category_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Category::find()->orderBy('name')->all(), 'id', 'name'),
        'options' => [ 'placeholder' => 'Category' ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    <?= $form->field($model, 'sell_category_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Category::find()->orderBy('name')->all(), 'id', 'name'),
        'options' => [ 'placeholder' => 'Category' ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    <?= $form->field($model, 'categories_form')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Category::find()->all(), 'id', 'name'),
            'options' => [ 'placeholder' => Yii::t('oteixido/bank', 'Sel·lecciona les categories'), 'multiple' => true ]
        ]);
    ?>
    <div class="form-group">
        <?= SubmitButtonWidget::widget(); ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
