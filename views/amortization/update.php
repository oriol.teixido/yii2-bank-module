<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Amortization */

$this->title = Yii::t('oteixido/bank', 'Modificar');
$this->params['breadcrumbs'][] = ['label' => Yii::t('oteixido/bank', 'Amortitzacions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="amortization-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
