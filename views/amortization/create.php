<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Amortization */

$this->title = Yii::t('oteixido/bank', 'Crear');
$this->params['breadcrumbs'][] = ['label' => Yii::t('oteixido/bank', 'Amortitzacions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="amortization-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
