<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;

use oteixido\gui\widgets\ModelActionsWidget;
use oteixido\gui\components\ModelActionColumn;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Amortitzacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="amortization-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= ModelActionsWidget::widget(['model' => $model]) ?>
    <div class="row">
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'name',
                    [
                        'label' => Yii::t('oteixido/bank', 'Període'),
                        'value' => function($model, $widget) {
                            return Yii::$app->formatter->format($model->start, 'date').' / '.Yii::$app->formatter->format($model->end, 'date');
                        },
                        'format' => 'raw'
                    ],
                    'buy:currency',
                    'cost:currency',
                    'sell:currency',
                    [
                        'label' => 'Total',
                        'value' => function($model, $widget) {
                            $cost = $model->buy + $model->cost + $model->sell;
                            $date1 = new \DateTime($model->start);
                            $date2 = new \DateTime($model->end);
                            $costMonth = $cost / $date1->diff($date2)->days * 30;
                            return Yii::$app->formatter->format($cost, 'currency').' (' . Yii::$app->formatter->format($costMonth, 'currency') .' / mes)';
                        }
                    ]
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => '{items}',
                'columns' => [
                    'date:date',
                    [
                        'attribute' => 'value',
                        'format' => 'currency',
                        'contentOptions' => [ 'style' => 'text-align:right' ],
                    ],
                    [
                        'attribute' => 'description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'category',
                        'value' => 'category.name',
                    ],
                    [
                        'class' => ModelActionColumn::className(),
                        'template' => '{update}',
                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action === 'update') {
                                return Url::to(['transaction/update', 'id' => $model->id]);
                            }
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
