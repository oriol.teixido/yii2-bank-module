<?php

use yii\helpers\Html;
use yii\grid\GridView;

use oteixido\gui\widgets\ModelActionsWidget;
use oteixido\gui\components\ModelActionColumn;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('oteixido/bank', 'Amortitzacions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="amortization-index">
    <?= ModelActionsWidget::widget() ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'name',
            [
                'class' => ModelActionColumn::className(),
                'template' => '{view}&nbsp;{update}&nbsp;{delete}',
                'actions' => [
                    'view' => [
                        'title' => Yii::t('oteixido/gui', 'Mostrar'),
                        'icon' => 'glyphicon-stats',
                    ],
                ],
            ],
        ],
    ]); ?>
</div>
