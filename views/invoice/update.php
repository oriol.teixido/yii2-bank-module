<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Invoice */

$this->title = Yii::t('oteixido/bank', 'Modificar');
$this->params['breadcrumbs'][] = ['label' => Yii::t('oteixido/bank', 'Factures'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invoice-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
