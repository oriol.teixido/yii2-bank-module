<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use kartik\file\FileInput;

use oteixido\gui\widgets\buttons\SubmitButtonWidget;
use oteixido\gui\widgets\ModelActionsWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Invoice */
/* @var $form yii\widgets\ActiveForm */

$boolValues = [
    '1' => Yii::$app->formatter->asBoolean(true) . ' (' . Yii::t('oteixido/bank', 'despesa'). ')',
    '0' => Yii::$app->formatter->asBoolean(false) . ' (' . Yii::t('oteixido/bank', 'ingrés'). ')',
];
?>

<div class="invoice-form">
    <?= ModelActionsWidget::widget(['model' => $model]) ?>
    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'options' => ['enctype' => 'multipart/form-data']]); ?>
    <?= $form->field($model, 'date')->widget(DatePicker::class, [
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>
    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'total')->textInput(['type' => 'float', 'maxlength' => true]) ?>
    <?= $form->field($model, 'vat')->textInput(['type' => 'float', 'maxlength' => true]) ?>
    <?= $form->field($model, 'expense')->dropdownList($boolValues) ?>
    <?= $form->field($model, 'attachmentFile')->widget(FileInput::classname(), [
        'options' => [ 'accept' => 'application/pdf' ],
        'pluginOptions' => [
            'showPreview' => true,
            'showUpload' => true,
            'initialPreviewAsData' => true,
            'initialPreviewFileType' => 'pdf',
            'initialPreview'=> $model->attachment ?  [ Url::to(['attachment', 'id' => $model->id ]) ] : [],
            'fileActionSettings' => [ 'showRemove' => false ],
        ],
        'pluginEvents' => [
            'filecleared'=> 'function() { $("#'.Html::getInputId($model, 'attachment').'").val(""); }',
            'fileloaded'=> 'function() { $("#'.Html::getInputId($model, 'attachment').'").val("-"); }',
        ],
    ]) ?>
    <?= $form->field($model, 'attachment')->hiddenInput()->label(false) ?>
    <div class="form-group">
        <?= SubmitButtonWidget::widget(); ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
