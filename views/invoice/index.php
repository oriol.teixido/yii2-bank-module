<?php
use yii\grid\GridView;
use yii\helpers\Html;

use oteixido\gui\widgets\ModelActionsWidget;
use oteixido\gui\components\ModelActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('oteixido/bank', 'Factures');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invoice-index">
    <?= ModelActionsWidget::widget() ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'date',
            'description',
            'vat:currency',
            'total:currency',
            'expense:boolean',
            [
                'label' => false,
                'format' => 'raw',
                'contentOptions' => [ 'style' => 'white-space: nowrap' ],
                'value' => function($data) {
                    $total = $data->expense ? -$data->total : $data->total;
                    if ($data->paid == $total) {
                        return '';
                    }
                    $paid = empty($data->paid) ? 0.0 : $data->paid;
                    return
                        '<span class="text-danger">'.
                        Yii::t('oteixido/bank', 'Pendent').' ('.Yii::$app->formatter->asCurrency(abs($total - $paid)).')'.
                        '</span>';
                },
            ],
            [ 'class' => ModelActionColumn::className() ],
        ],
    ]); ?>
</div>
