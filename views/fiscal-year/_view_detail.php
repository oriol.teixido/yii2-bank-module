<?php

use yii\widgets\DetailView;

use oteixido\bank\models\Transaction;

/* @var $this yii\web\View */
/* @var $model app\models\FiscalYearBudget */
?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        [
            'label' => Yii::t('oteixido/bank', 'Període'),
            'value' => function($model, $widget) {
                return Yii::$app->formatter->format($model->start, 'date').' / '.Yii::$app->formatter->format($model->end, 'date');
            },
            'format' => 'raw',
        ],
        [
            'label' => Yii::t('oteixido/bank', 'Pressupost'),
            'value' => $model->getBudgets()->sum('income') - $model->getBudgets()->sum('expense'),
            'format' => 'currency',
        ],
        [
            'label' => Yii::t('oteixido/bank', 'Balanç'),
            'value' => Transaction::find()->between($model->start, $model->end, 'date_fiscal')->value(),
            'format' => 'currency',

        ],
    ],
]) ?>
