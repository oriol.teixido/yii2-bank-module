<?php

use yii\helpers\Html;

use oteixido\gui\widgets\ModelActionsWidget;

/* @var $this yii\web\View */
/* @var $model oteixido\bank\models\FiscalYear
/* @var $budget oteixido\bank\models\FiscalYearBudget */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $categories[] oteixido\bank\models\Category */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('oteixido/bank', 'Anys fiscals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="fiscal-year-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= ModelActionsWidget::widget(['model' => $model]) ?>

    <div class="row">
        <div class="col-md-12">
            <?= $this->render('_view_detail', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

    <?php if (count($categories) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-warning" role="alert">
                <p><?= Yii::t('oteixido/bank', 'Hi ha categories amb imports pendents d\'assignar.') ?></p>
                <p>&nbsp;</p>
                <div class="fiscal-year-budget-create">
                    <?= $this->render('_form-budget', [
                        'model' => $budget,
                        'categories' => $categories,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-md-12">
            <?= $this->render('_view_budget', [
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $this->render('_view_expense', [
                'dataProvider' => $dataProvider,
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $this->render('_view_income', [
                'dataProvider' => $dataProvider,
            ]) ?>
        </div>
    </div>
</div>
