<?php

use yii\grid\GridView;

use oteixido\gui\components\ModelActionColumn;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$totalIncome = array_reduce($dataProvider->models, function($carry, $item) { return $carry + $item->income; });
$totalExpense = array_reduce($dataProvider->models, function($carry, $item) { return $carry + $item->expense; });
$totalCurrentIncome = array_reduce($dataProvider->models, function($carry, $item) { return $carry + $item->currentIncome; });
$totalCurrentExpense = array_reduce($dataProvider->models, function($carry, $item) { return $carry + $item->currentExpense; });
?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'layout' => '{items}',
    'showFooter' => true,
    'footerRowOptions' => ['style'=>'font-weight:bold;text-align:right;'],
    'columns' => [
        [
            'attribute' => 'category',
            'value' => 'category.name',
        ],
        [
            'attribute' => 'income',
            'format' => 'currency',
            'footer' => Yii::$app->formatter->asCurrency($totalIncome),
            'contentOptions' => [ 'style' => 'text-align:right' ],
        ],
        [
            'attribute' => 'expense',
            'format' => 'currency',
            'footer' => Yii::$app->formatter->asCurrency($totalExpense),
            'contentOptions' => [ 'style' => 'text-align:right' ],
        ],
        [
            'attribute' => 'currentIncome',
            'format' => 'currency',
            'footer' => Yii::$app->formatter->asCurrency($totalCurrentIncome),
            'contentOptions' => [ 'style' => 'text-align:right' ],
        ],
        [
            'attribute' => 'currentExpense',
            'format' => 'currency',
            'footer' => Yii::$app->formatter->asCurrency($totalCurrentExpense),
            'contentOptions' => [ 'style' => 'text-align:right' ],
        ],
        [
            'attribute' => 'deviation',
            'label' => Yii::t('oteixido/bank', 'Desviació'),
            'value' => function($model) {
                return ($model->currentIncome - $model->currentExpense) - ($model->income - $model->expense);
            },
            'format' => 'currency',
            'footer' => Yii::$app->formatter->asCurrency(($totalCurrentIncome - $totalCurrentExpense) - ($totalIncome - $totalExpense)),
            'contentOptions' => [ 'style' => 'text-align:right' ],
        ],
        [
            'class' => ModelActionColumn::className(),
            'urlCreator' => function ( $action,  $model,  $key,  $index) {
                return [$action.'-budget', 'id' => $key];
            },
        ],
    ],
]); ?>
