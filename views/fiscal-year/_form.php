<?php

use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;

use oteixido\gui\widgets\buttons\SubmitButtonWidget;
use oteixido\gui\widgets\ModelActionsWidget;

/* @var $this yii\web\View */
/* @var $model app\models\FiscaYear */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fiscal-year-form">
    <?= ModelActionsWidget::widget(['model' => $model]) ?>
    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'start')->widget(DatePicker::class, [
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>
    <?= $form->field($model, 'end')->widget(DatePicker::class, [
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>
    <div class="form-group">
        <?= SubmitButtonWidget::widget(); ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
