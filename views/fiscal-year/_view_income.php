<?php

use miloschuman\highcharts\Highcharts;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */

$income = array_values(array_filter($dataProvider->models, function($o) { return $o->currentIncome != 0; }));
$income = array_map(function ($o) { return [ 'name' => $o->category->name, 'y' => (float)$o->currentIncome ]; }, $income);
?>
<?= Highcharts::widget([
        'options' => [
            'title' => [ 'text' => Yii::t('oteixido/bank', 'Income')],
            'series' => [
                [ 'type' => 'pie', 'data' => $income ]
            ]
        ]
    ]);
?>
