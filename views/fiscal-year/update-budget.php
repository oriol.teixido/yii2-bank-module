<?php

/* @var $this yii\web\View */
/* @var $model app\models\FiscaYearBudget */
/* @var $categories[] oteixido\bank\models\Category */

$this->title = Yii::t('oteixido/bank', 'Modificar pressupost');
$this->params['breadcrumbs'][] = ['label' => Yii::t('oteixido/bank', 'Anys fiscals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fiscalYear->name, 'url' => ['view', 'id' => $model->fiscalYear->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fiscal-year-budget-update">
    <?= $this->render('_form-budget', [
        'model' => $model,
        'categories' => $categories,
    ]) ?>
</div>
