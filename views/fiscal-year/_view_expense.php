<?php

use miloschuman\highcharts\Highcharts;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */

$expense = array_values(array_filter($dataProvider->models, function($o) { return $o->currentExpense != 0; }));
$expense = array_map(function ($o) { return [ 'name' => $o->category->name, 'y' => (float)$o->currentExpense ]; }, $expense);
?>
<?= Highcharts::widget([
        'options' => [
            'title' => [ 'text' => Yii::t('oteixido/bank', 'Expense')],
            'series' => [
                [ 'type' => 'pie', 'data' => $expense ]
            ]
        ]
    ]);
?>
