<?php

use yii\helpers\Html;
use yii\grid\GridView;

use oteixido\gui\widgets\ModelActionsWidget;
use oteixido\gui\components\ModelActionColumn;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('oteixido/bank', 'Anys fiscals');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fiscal-year-index">
    <?= ModelActionsWidget::widget() ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'name',
            [
                'attribute' => 'start',
                'format' => 'date',
                'contentOptions' => [ 'style' => 'text-align:right' ],
            ],
            [
                'attribute' => 'end',
                'format' => 'date',
                'contentOptions' => [ 'style' => 'text-align:right' ],
            ],
            [
                'attribute' => 'income',
                'format' => 'currency',
                'contentOptions' => [ 'style' => 'text-align:right' ],
            ],
            [
                'attribute' => 'expense',
                'format' => 'currency',
                'contentOptions' => [ 'style' => 'text-align:right' ],
            ],
            [
                'attribute' => 'balance',
                'format' => 'currency',
                'contentOptions' => [ 'style' => 'text-align:right' ],
            ],
            [
                'class' => ModelActionColumn::className(),
                'template' => '{view}&nbsp;{update}&nbsp;{delete}',
                'actions' => [
                    'view' => [
                        'title' => Yii::t('oteixido/gui', 'Mostrar'),
                        'icon' => 'glyphicon-stats',
                    ],
                ],
            ],
        ],
    ]); ?>
</div>
