<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;

use oteixido\gui\widgets\buttons\SubmitButtonWidget;

/* @var $this yii\web\View */
/* @var $model app\models\FiscaYearBudget */
/* @var $categories[] oteixido\bank\models\Category */
?>

<div class="fiscal-year-budget-form">
    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
        <?= Html::activeHiddenInput($model, 'fiscal_year_id') ;?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'category_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map($categories, 'id', 'name'),
                    'options' => [ 'placeholder' => Yii::t('oteixido/bank', 'Selecciona una categoria...') ],
                ])->label(true); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'income')->textInput(['maxlength' => true])->label(true) ?>
                <?= $form->field($model, 'expense')->textInput(['maxlength' => true])->label(true) ?>
            </div>
        </div>
        <?= SubmitButtonWidget::widget(); ?>
    <?php ActiveForm::end(); ?>
</div>
