<?php

/* @var $this yii\web\View */
/* @var $model app\models\FiscaYear */

$this->title = Yii::t('oteixido/bank', 'Crear');
$this->params['breadcrumbs'][] = ['label' => Yii::t('oteixido/bank', 'Anys fiscals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fiscal-year-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
