<?php

/* @var $this yii\web\View */
/* @var $model app\models\FiscaYear */

$this->title = Yii::t('oteixido/bank', 'Modificar');
$this->params['breadcrumbs'][] = ['label' => Yii::t('oteixido/bank', 'Anys fiscals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fiscal-year-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
