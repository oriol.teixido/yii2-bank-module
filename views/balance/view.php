<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use miloschuman\highcharts\Highcharts;

use oteixido\gui\widgets\ModelActionsWidget;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('oteixido/bank', 'Balanços'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="balance-view">
    <?= ModelActionsWidget::widget(['model' => $model]) ?>
    <div class="row">
        <div class="col-md-6">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => '{items}',
                'columns' => [
                    [
                        'label' => Yii::t('oteixido/bank', 'Any'),
                        'attribute' => 'year',
                    ],
                    [
                        'label' => Yii::t('oteixido/bank', 'Total'),
                        'attribute' => 'value',
                        'format' => 'currency',
                        'contentOptions' => [ 'style' => 'text-align:right' ],
                    ],
                    [
                        'label' => Yii::t('oteixido/bank', 'Acumulat'),
                        'attribute' => 'total',
                        'format' => 'currency',
                        'contentOptions' => [ 'style' => 'text-align:right' ],
                        'visible' => $model->accumulate,
                    ],
                ],
            ]); ?>
        </div>
        <div class="col-md-6">
            <?= Highcharts::widget([
                    'options' => [
                        'title' => false,
                        'xAxis' => [
                            'categories' => array_reverse(ArrayHelper::getColumn($dataProvider->getModels(), 'year')),
                        ],
                        'yAxis' => [
                            'title' => false,
                        ],
                        'series' => array_merge(
                            [
                                ['name' => 'Total', 'data' => array_reverse(ArrayHelper::getColumn($dataProvider->getModels(), 'value'))],
                            ],
                            $model->accumulate ? [
                                ['name' => 'Acumulat', 'data' => array_reverse(ArrayHelper::getColumn($dataProvider->getModels(), 'total'))],
                            ] : [])
                    ]
                ]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <?php
            $monthColumns = [];
            for($month=1; $month<=12; $month++) {
                $monthColumns[] = [
                    'label' => str_pad((int)$month, 2, '0', STR_PAD_LEFT),
                    'attribute' => $month,
                    'format' => 'currency',
                    'contentOptions' => [ 'style' => 'text-align:right' ],
                ];
            }
        ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => '{items}',
            'columns' => array_merge([
                [
                    'label' => 'Any',
                    'attribute' => 'year',
                ]
            ], $monthColumns),
        ]); ?>
    </div>
</div>
