<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use oteixido\gui\widgets\buttons\SubmitButtonWidget;
use oteixido\gui\widgets\ModelActionsWidget;

use oteixido\bank\models\Category;

/* @var $this yii\web\View */
/* @var $model app\models\Balance */
/* @var $form yii\widgets\ActiveForm */

$boolValues = [
    '0' => Yii::$app->formatter->asBoolean(false),
    '1' => Yii::$app->formatter->asBoolean(true),
];
?>

<div class="balance-form">
    <?= ModelActionsWidget::widget(['model' => $model]) ?>
    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'start')->widget(DatePicker::class, [
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>
    <?= $form->field($model, 'end')->widget(DatePicker::class, [
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>
    <?= $form->field($model, 'categories_form')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Category::find()->all(), 'id', 'name'),
            'options' => [ 'placeholder' => Yii::t('oteixido/bank', 'Sel·lecciona les categories'), 'multiple' => true ]
        ]);
    ?>
    <?= $form->field($model, 'accumulate')->dropdownList($boolValues) ?>
    <?= $form->field($model, 'inverse')->dropdownList($boolValues) ?>
    <div class="form-group">
        <?= SubmitButtonWidget::widget(); ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
