<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Transaction */
/* @var $modelNew app\models\Transaction */

$this->title = Yii::t('oteixido/bank', 'Dividir');
$this->params['breadcrumbs'][] = ['label' => Yii::t('oteixido/bank', 'Moviments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaction-split">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'date',
            'value:currency',
            'description:html',
        ],
    ]) ?>
    <?= $this->render('_form', [
        'model' => $modelNew,
    ]) ?>
</div>
