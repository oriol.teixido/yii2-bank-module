<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use kartik\select2\Select2;

use oteixido\bank\models\Account;
use oteixido\bank\models\Category;
use oteixido\bank\models\Invoice;
use oteixido\gui\widgets\buttons\SubmitButtonWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Transaction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transaction-form">
    <?php if (!$model->isNewRecord): ?>
        <?= Html::a(Yii::t('oteixido/bank', 'Duplicar'), ['duplicate', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('oteixido/bank', 'Dividir'), ['split', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('oteixido/bank', 'Esborrar'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('oteixido/bank', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    <?php endif; ?>
    <p>&nbsp;</p>
    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'date')->widget(DatePicker::class, [
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>

    <?= $form->field($model, 'date_fiscal')->widget(DatePicker::class, [
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>

    <?= $form->field($model, 'value')->textInput(['type' => 'float', 'maxlength' => true]) ?>

    <?= $form->field($model, 'account_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Account::find()->orderBy('name')->all(), 'id', 'name'),
        'options' => [ 'placeholder' => Yii::t('oteixido/bank', 'Compte') ],
    ]); ?>

    <?= $form->field($model, 'category_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Category::find()->orderBy('name')->all(), 'id', 'name'),
        'options' => [ 'placeholder' => Yii::t('oteixido/bank', 'Categoria') ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'invoice_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Invoice::find()->orderBy('date')->all(), 'id', 'name'),
        'options' => [ 'placeholder' => Yii::t('oteixido/bank', 'Factura') ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'validated')->dropdownList([
        '0' => Yii::t('oteixido/bank', 'No validat'),
        '1' => Yii::t('oteixido/bank', 'Validat'),
    ]) ?>

    <?= $form->field($model, 'description')->widget(\yii\redactor\widgets\Redactor::className(), [
            'clientOptions' => [ 'minHeight' => 200 ],
        ]) ?>

    <div class="form-group">
        <?= SubmitButtonWidget::widget(); ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
