<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;
use kartik\export\ExportMenu;

use oteixido\bank\models\TransactionMassiveForm;
use oteixido\gui\components\ModelActionColumn;
use oteixido\gui\widgets\ModelActionsWidget;
use oteixido\gui\widgets\buttons\ButtonWidget;
use oteixido\gui\widgets\buttons\SubmitButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('oteixido/bank', 'Moviments');
$this->params['breadcrumbs'][] = $this->title;

$dataProvider->setPagination(['pageSize' => 50]);
?>
<div class="transaction-index">
    <?= ModelActionsWidget::widget(['actions' => [
        ButtonWidget::widget([
            'actionId' => 'import',
            'title' => Yii::t('oteixido/bank', 'Importar'),
            'icon' => 'glyphicon-import',
        ]),
    ]]) ?>
    <?= Html::beginForm(['massive']); ?>
    <div style="text-align:left">
        <?= Html::dropDownList('TransactionMassiveForm[action]', $selection = null, $items = [
            TransactionMassiveForm::ACTION_FILTER => Yii::t('oteixido/bank', 'Aplicar filtres'),
            TransactionMassiveForm::ACTION_VALIDATE => Yii::t('oteixido/bank', 'Validar'),
            TransactionMassiveForm::ACTION_DELETE => Yii::t('oteixido/bank', 'Esborrar'),
        ], $options = [
            'prompt' => Yii::t('oteixido/bank', '-- Acció sobre la sel·lecció --'),
        ])?>
        <?= SubmitButtonWidget::widget([
            'title' => Yii::t('oteixido/bank', 'Aplicar'),
            'htmlOptions' => [
                'data' => [
                    'confirm' => Yii::t('oteixido/bank', 'L\'acció s\'aplicarà als elements seleccionats. Continuar?'),
                    'method' => 'post',
                ],
            ],
        ]); ?>
    </div>
    <div>&nbsp;</div>
    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
                'date:date',
                'date_fiscal:date',
                'value:currency',
                [
                    'attribute' => 'description',
                    'value' => function ($row) {
                        return strip_tags($row->description);
                    },
                ],
                [
                    'attribute' => 'account',
                    'value' => 'account.name',
                ],
                [
                    'attribute' => 'category',
                    'value' => 'category.name',
                ],
            ],
    ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'name' => 'TransactionMassiveForm[selected]',
            ],
            'date:date',
            'date_fiscal:date',
            'moved:boolean',
            [
                'attribute' => 'value',
                'format' => 'currency',
                'contentOptions' => [ 'style' => 'text-align:right' ],
            ],
            [
                'attribute' => 'description',
                'value' => function ($model) {
                    $len = 40;
                    $text = strip_tags($model->description);
                    return (strlen($text) > $len) ? substr($text,0,$len-3).'...' : $text;
                },
                'contentOptions' => [ 'style' => 'white-space: nowrap' ],
            ],
            [
                'attribute' => 'account',
                'value' => 'account.name',
            ],
            [
                'attribute' => 'category',
                'value' => 'category.name',
            ],
            [
                'attribute' => 'invoice',
                'format' => 'raw',
                'value' => function($data) {
                    if ($data->invoice) {
                        return Html::a('<span class="btn btn-sm btn-success glyphicon glyphicon-inbox"></span>', Url::to(['invoice/update', 'id' => $data->invoice->id]));
                    }
                    return '';
                },
            ],
            'validated:boolean',
            [
                'class' => ModelActionColumn::className(),
                'template' => '{update}&nbsp;&nbsp;{duplicate}&nbsp;&nbsp;{split}&nbsp;&nbsp;{delete}',
                'actions' => [
                    'duplicate' => [
                        'title' => Yii::t('oteixido/bank', 'Duplicar'), 'icon' => 'glyphicon-duplicate',
                    ],
                    'split' => [
                        'title' => Yii::t('oteixido/bank', 'Dividir'), 'icon' => 'glyphicon-scissors'
                    ],
                ],
            ],
        ],
        'rowOptions' => function ($model) {
            if (!$model->validated) {
                return ['class' => 'danger'];
            }
        },
        'tableOptions' => ['class' => 'table table-bordered'],
    ]); ?>
    <?= Html::endForm(); ?>
</div>
