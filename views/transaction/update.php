<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Transaction */

$this->title = Yii::t('oteixido/bank', 'Modificar');
$this->params['breadcrumbs'][] = ['label' => Yii::t('oteixido/bank', 'Moviments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaction-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
