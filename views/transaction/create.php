<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Transaction */

$this->title = Yii::t('oteixido/bank', 'Crear');
$this->params['breadcrumbs'][] = ['label' => Yii::t('oteixido/bank', 'Moviments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaction-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
