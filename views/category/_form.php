<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

use oteixido\gui\widgets\buttons\SubmitButtonWidget;
use oteixido\gui\widgets\ModelActionsWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">
    <?= ModelActionsWidget::widget(['model' => $model]) ?>
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'balanced')->dropdownList([
        '0' => Yii::t('oteixido/bank', 'No'),
        '1' => Yii::t('oteixido/bank', 'Sí'),
    ]) ?>
    <div class="form-group">
        <?= SubmitButtonWidget::widget(); ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
