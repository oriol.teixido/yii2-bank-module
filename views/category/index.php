<?php

use yii\helpers\Html;
use yii\grid\GridView;

use oteixido\gui\widgets\ModelActionsWidget;
use oteixido\gui\components\ModelActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('oteixido/bank', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">
    <?= ModelActionsWidget::widget() ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'name',
            'balanced:boolean',
            ['class' => ModelActionColumn::className()],
        ],
    ]); ?>
</div>
