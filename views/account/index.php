<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use oteixido\gui\widgets\ModelActionsWidget;
use oteixido\gui\components\ModelActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AccountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('oteixido/bank', 'Comptes');
$this->params['breadcrumbs'][] = $this->title;

$total = array_sum(ArrayHelper::getColumn($dataProvider->models, 'balance'));
$total = Yii::$app->formatter->asCurrency($total);
$dataProvider->setPagination(false);
?>
<div class="account-index">
    <?= ModelActionsWidget::widget() ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showFooter' => true,
        'footerRowOptions'=>[ 'style' => 'font-weight:bold;text-align:right' ],
        'columns' => [
            'name',
            'iban',
            'description',
            [
                'attribute' => 'balance',
                'format' => 'currency',
                'contentOptions' => [ 'style' => 'text-align:right' ],
                'footer' => $total,
            ],
            [ 'class' => ModelActionColumn::className() ],
        ],
    ]); ?>
</div>
