<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Account */

$this->title = Yii::t('oteixido/bank', 'Modificar');
$this->params['breadcrumbs'][] = ['label' => Yii::t('oteixido/bank', 'Comptes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="account-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
