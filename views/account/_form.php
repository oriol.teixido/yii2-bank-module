<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

use oteixido\gui\widgets\buttons\SubmitButtonWidget;
use oteixido\gui\widgets\ModelActionsWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Account */
?>

<div class="account-form">
    <?= ModelActionsWidget::widget(['model' => $model]) ?>
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'iban')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
    <div class="form-group">
        <?= SubmitButtonWidget::widget(); ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
