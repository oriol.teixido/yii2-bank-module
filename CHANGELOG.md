2.6.0 / 2020-01-21
==================

  * Afegir IBAN a Account
  * FIX. Visualització fitxers pdf amb ruta relativa

2.5.1 / 2019-09-30
==================

  * Fix. Mostrar tots els elements quan no hi ha paginació

2.4.0 / 2019-03-01
==================

  * Mostrar moviments amb data fiscal modificada
  * Afegir balancos totals a fiscal-year/view

2.3.1 / 2019-02-24
==================

  * Fix. Issue#5 Arror al afegir income i expense nulls.

2.3.0 / 2019-02-24
==================

   * Implementar funcionalitat anys fiscals


2.2.0 / 2019-01-29
==================

   * Implementar funcionalitat factures pendents

2.1.0 / 2019-01-18
==================

   * Implementar funcionalitat FiscalYear

2.0.3 / 2018-12-23
==================

   * Fix Issue#3 Mesos als balanços incorrectes
   * Fix Issue#4 Amortization error

 2.0.2 / 2018-12-22
 ==================

   * Fix Guardar models al importar des d'un fitxer.

2.0.1 / 2018-12-10
==================

   * Fix Issue#2. Assignació automàtica d'etiquetes

2.0   / 2018-12-09
==================

   * Gestió de Categories
   * Gestió de Filtres
   * Gestió de Balanços
   * Gestió d'Amortitzacions

1.1   / 2018-11-07
==================

   * Importació de fitxers csv

1.0   / 2018-11-07
==================

   * Gestió de Moviments
   * Gestió de Factures
   * Gestió de comptes
